import Head from 'next/head'

const HelloNext = () => {
    return (
        <>
            <Head>
                <title>
                    Hello Next.js
                </title>
                <meta charSet='utf-8'></meta>
            </Head>
        </>
    )
}

export default HelloNext