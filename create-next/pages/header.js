import HelloNext from '../components/HelloNext'
import '../public/test.css'
import { Button } from 'antd'

function Header(){
    return (
        <>
            <HelloNext />
            <div>Hello Next.js</div>
            <div><Button>Button</Button></div>
        </>
        
    )
}

export default Header