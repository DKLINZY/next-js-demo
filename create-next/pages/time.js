import { useState } from "react"
import dynamic from "next/dynamic"

//Lazy Loading moment(module) & component

const LazyLoadingDemo = dynamic(import('../components/LazyLoadingDemo'))

function Time(){
    const [now, setTime] = useState(Date.now())
    const changetime = async () => {
        const moment = await import('moment')
        setTime(moment.default(Date.now()).format())
    }


    return (
        <>
            <div>Time is: {now}</div>
            <LazyLoadingDemo />
            <div><button onClick={changetime}>Change Time Format</button></div>
        </>
    )
}

export default Time