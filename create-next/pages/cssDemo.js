import { useState } from "react"

function CssDemo(){

    const [color, setColor] = useState('blue')
    
    const changeColor = () => {
        setColor(color=='blue'?'red':'blue')
    }

    return (
        <>
            <div>CSS Demo Page</div>
            <div><button onClick={changeColor}>Change Color</button></div>

            <style jsx>
                {`
                    div{
                        color:${color};
                    }
                `}
            </style>
        </>
    )
}

export default CssDemo