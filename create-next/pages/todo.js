import { withRouter } from 'next/router'
import Link from 'next/link'
import axios from 'axios'

function Todo({ router, list }){
    return (
        <>
            <div>{router.query.name}</div>
            <div>{list.join(', ')}</div>
            <Link href='/'><a>Index</a></Link>
        </>
    )
}

export async function getStaticProps() {
    const res = await axios('http://rap2.taobao.org:38080/app/mock/245501/redux-demo')
    return await {
        props: {
            list: res.data.data.list
        }
    }
}

export default withRouter(Todo)