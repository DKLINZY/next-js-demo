import { withRouter } from 'next/router'
import Link from 'next/link'
import axios from 'axios'

function Person({router, list}){
    return (
        <>
            <div>{router.query.name}</div>
            <div>{list}</div>
            <Link href='/'><a>Index</a></Link>
        </>
    )
}

Person.getInitialProps = async () => {
    const res = await axios('http://rap2.taobao.org:38080/app/mock/245501/redux-demo')
    console.log(res)
    return await res.data.data
}

export default withRouter(Person)