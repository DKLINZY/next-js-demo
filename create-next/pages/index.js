import React from 'react'
import Link from 'next/link'
import Router from 'next/router'

const Home = () => {

    //Router events listener
    //usage: loading animation, distory components

    //routeChangeStart
    Router.events.on('routeChangeStart', (...args)=>{
        console.log('1. route change start, args:',...args)
    })

    //routeChangeComplete
    Router.events.on('routeChangeComplete', (...args)=>{
        console.log('2. route change complete, args:',...args)
    })

    //beforeHistoryChange
    Router.events.on('beforeHistoryChange', (...args)=>{
        console.log('3. before history change, args:',...args)
    })

    //routeChangeError
    Router.events.on('routeChangeError', (...args)=>{
        console.log('4. route change error, args:',...args)
    })

    //hashChangeStart
    Router.events.on('hashChangeStart', (...args)=>{
        console.log('5. hash change start, args:',...args)
    })

    //hashChangeComplete
    Router.events.on('hashChangeComplete', (...args)=>{
        console.log('6. hash change complete, args:',...args)
    })
    

    const goto = () => {
        Router.push('/helloA')
    }

    const gotoPersonA = () => {
        // Router.push('/person?name=PersonA')
        Router.push({
            pathname: '/person',
            query: {
                name: 'PersonA'
            }
        })
    }

    return (
        <>
            <div>This is index page</div>
            <div><Link href="/helloA"><a>Hello A</a></Link></div>
            <div><Link href="/helloB"><a>Hello B</a></Link></div>
            <div>
                <button onClick={goto}>Hello A</button>
            </div>

            <br />
            <br />

            <div>
                <Link href='/person?name=PersonA'><a>PersonA</a></Link>
                <br />
                <Link href={{pathname:'/person', query:{name:'PersonB'}}}><a>PersonB</a></Link>
            </div>
            <div>
                <button onClick={gotoPersonA}>go to Person A page</button>
            </div>
            <div>
                <Link href='#hash'><a>Hash</a></Link>
            </div>
            <div>
                <Link href='/todo?name=TodoList'><a>Todo List</a></Link>
            </div>
        </>
    )
}

export default Home